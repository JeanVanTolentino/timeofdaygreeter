export class DayGreeter {
    greet(name?:string){
        let date = new Date()
        let time = date.getHours()

        if(name){
            return greeting() +', '+ name
        }else {
            return greeting()
        }

        function greeting(){
            if (time>=5 && time<=11){
                return `Good morning`
            } else if (time>=12 && time <=16) {
                return `Good afternoon`
            } else {
                return `Good evening`
            }
        }
    }
}