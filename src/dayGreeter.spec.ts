import { DayGreeter } from "./dayGreeter";

describe('DayGreeter', ()=>{
    describe('greet', ()=>{
        it('must return appropriate greeting', ()=>{
            let dayGreeter = new DayGreeter
            let greeting = dayGreeter.greet('jam')
            expect(greeting).toEqual('Good afternoon, jam')
        })
    })
})